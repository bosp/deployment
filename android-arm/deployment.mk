.PHONY: build-update-zip
ifdef CONFIG_DEPLOYMENT_BUILD_UPDATE_ZIP
deployment: build-update-zip
endif
ifdef CONFIG_DEPLOYMENT_APPLY_UPDATE_ZIP
deployment: apply-update-zip
endif
export SRC=$(BUILD_DIR)
export DEST=$(BASE_DIR)/deployment/android-arm/update_zip
export STRIP=$(shell echo $(CXX) |sed -f deployment/android-arm/strip_from_g++.sed)
export DEVDIR=$(CONFIG_BOSP_RUNTIME_PATH)
export RWPATH=$(CONFIG_BOSP_RUNTIME_RWPATH)
build-update-zip: build
	@echo STRIP $(STRIP)
	@deployment/$(PLATFORM)/extract_image
	@deployment/$(PLATFORM)/gen_update

apply-update-zip: build-update-zip $(shell echo $(CONFIG_DEPLOYMENT_ANDROID_SDK_PATH)/platform-tools/adb |tr -d \")
	@deployment/$(PLATFORM)/apply_update
	
